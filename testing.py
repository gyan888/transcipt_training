import os
import dialogflow_v2 as dialogflow
import random
from collections import defaultdict
import pandas as pd
dd=defaultdict(list)


project_ids={
        "MWCC_pass_reset_first":"mwcc-pass-reset-first-3f770",
        "MWCC_forget_username":"mwcc-forget-username-5e545",
        "MWCC_login_Staff":"mwcc-login-staff-71bd0",
        "MWCC_login_Student":"mwcc-login-student-da141",
        "MWCC_login_Faculty":"mwcc-login-faculty-66db9",
        "MWCC_advert":"mwcc-advert-7c9a9",
        "Smalltalk_Agent":"smalltalk-agent-b7ae8"
    }




def jsonV2_parser_apiai(response):
    try:
        json_resp = {}
        json_resp["intent_name"] = response.query_result.intent.display_name
        json_resp["apiai_response"] = response.query_result.fulfillment_text
        json_resp["score"] = response.query_result.intent_detection_confidence
        json_resp["action_name"] = response.query_result.fulfillment_text
        json_resp["entities"] = response.query_result.parameters
        json_resp["contexts"] = response.query_result.output_contexts
        return json_resp
    except Exception as e:
        print e
        json_resp = {}
        json_resp["intent_name"]="Exception"
        json_resp["score"] = "Exception"
        return json_resp


def detect_intent_texts(session_id, texts, project_id, ai_agent):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "C:\Users\gyanender.kumar\Documents\DgFlowCredentials\{}.json".format(ai_agent)
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(project_id, session_id)
    print('Session path: {}\n'.format(session))

    for text in texts:
        text_input = dialogflow.types.TextInput(
            text=text, language_code='en-US')

        query_input = dialogflow.types.QueryInput(text=text_input)

        response = session_client.detect_intent(
            session=session, query_input=query_input)


    if os.environ.has_key("GOOGLE_APPLICATION_CREDENTIALS"):
        os.environ.pop("GOOGLE_APPLICATION_CREDENTIALS")
    return jsonV2_parser_apiai(response)



def testCase(fileName,ai_agent):
    global project_ids
    global dd
    path='C:\\Users\\gyanender.kumar\\BotTesting\\transcript_data\\Userquerry\{}.text'.format(fileName)
    with open(path,'r') as f:
        all_data=f.readlines()

    for text in all_data:
        response=detect_intent_texts(random.randrange(1,99999999), [text], project_ids[ai_agent], ai_agent)
        dd['UserQuerry'].append(text)
        dd['recognized_intent'].append(response["intent_name"])
        dd['Confidence'].append(response["score"])
    dataframe = pd.DataFrame.from_dict(dd, orient='index')
    dataframe = dataframe.transpose()
    dataframe.to_csv(fileName+'test.csv', sep=',', encoding='utf-8')

    print dataframe



testCase("pass","MWCC_pass_reset_first")